import { SequelizeShippingCompanyRepository } from '../../repositories/implementations/SequelizeShippingCompanyRepository'
import { CreateShippingCompanyController } from './CreateShippingCompanyController'
import { CreateShippingCompanyUseCase } from './CreateShippingCompanyUseCase'
import { HashService } from '../../services/HashService'
import { TokenService } from '../../services/TokenService'

const sequelizeShippingCompanyRepository =
  new SequelizeShippingCompanyRepository()

const hashService = new HashService()

const tokenService = new TokenService()

const createShippingCompanyUseCase = new CreateShippingCompanyUseCase(
  hashService,
  sequelizeShippingCompanyRepository,
  tokenService
)

const createShippingCompanyController = new CreateShippingCompanyController(
  createShippingCompanyUseCase
)

export { createShippingCompanyUseCase, createShippingCompanyController }

import { CreateShippingCompanyRequestDTO } from './CreateShippingCompanyDTO'
import { HashService } from '../../services/HashService'
import { SequelizeShippingCompanyRepository } from '../../repositories/implementations/SequelizeShippingCompanyRepository'
import { TokenService } from '../../services/TokenService'

export class CreateShippingCompanyUseCase {
  constructor(
    private hashService: HashService,
    private sequelizeShippingCompanyRepository: SequelizeShippingCompanyRepository,
    private tokenService: TokenService
  ) {}

  async execute(data: CreateShippingCompanyRequestDTO) {
    const ShippingCompanyExists =
      await this.sequelizeShippingCompanyRepository.findByCNPJ(data.cnpj)

    if (ShippingCompanyExists) {
      if (ShippingCompanyExists.stateRegistration === data.stateRegistration) {
        throw new Error('this State Registration already been used')
      }

      if (ShippingCompanyExists.contact === data.contact) {
        throw new Error('this contact already been used')
      }

      throw new Error('this CNPJ already been used')
    }

    const passwordHash = await this.hashService.hash(data.password)

    const shippingCompany = await this.sequelizeShippingCompanyRepository.save({
      cnpj: data.cnpj,
      name: data.name,
      corporateName: data.corporateName,
      address: data.address,
      stateRegistration: data.stateRegistration,
      contact: data.contact,
      password: passwordHash,
    })

    const token = this.tokenService.generate(data.cnpj)

    return { shippingCompany, token }
  }
}

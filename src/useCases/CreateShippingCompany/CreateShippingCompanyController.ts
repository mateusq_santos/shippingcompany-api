import { Request, Response } from 'express'
import { CreateShippingCompanyUseCase } from './CreateShippingCompanyUseCase'
import { shippingCompanyValidator } from '../../Validations/ShippingCompanyValidation'

export class CreateShippingCompanyController {
  constructor(private createShippingCompany: CreateShippingCompanyUseCase) {}

  async execute(request: Request, response: Response): Promise<Response> {
    const {
      cnpj,
      name,
      corporateName,
      address,
      stateRegistration,
      contact,
      password,
    } = request.body

    try {
      await shippingCompanyValidator.validate(request.body)
      await this.createShippingCompany.execute({
        cnpj,
        name,
        corporateName,
        address,
        stateRegistration,
        contact,
        password,
      })

      return response.status(201).json('User created with success !')
    } catch (err) {
      return response.status(400).json({
        message: err.message || 'Something went wrong',
      })
    }
  }
}

export class CreateShippingCompanyRequestDTO {
  cnpj: string
  name: string
  corporateName: string
  address: string
  stateRegistration: string
  contact: string
  password: string
}

import { HashService } from '../../services/HashService'
import { SequelizeShippingCompanyRepository } from '../../repositories/implementations/SequelizeShippingCompanyRepository'
import { TokenService } from '../../services/TokenService'
import { AuthenticatedShippingCompanyRequestDTO } from './AuthenticatedShippingCompanyRequestDTO'

export class AuthenticateShippingCompanyUseCase {
  constructor(
    private hashService: HashService,
    private sequelizeShippingCompanyRepository: SequelizeShippingCompanyRepository,
    private tokenService: TokenService
  ) {}

  async execute(data: AuthenticatedShippingCompanyRequestDTO) {
    const shippingCompanyData =
      await this.sequelizeShippingCompanyRepository.findByCNPJ(data.cnpj)

    if (!shippingCompanyData) {
      throw new Error('User Not Found')
    }

    const isTheSamePassword = await this.hashService.compare(
      data.password,
      shippingCompanyData.password
    )

    if (!isTheSamePassword) {
      throw new Error('The password is incorrect')
    }

    const token = this.tokenService.generate(shippingCompanyData.id)

    return {
      shippingCompanyData,
      token,
    }
  }
}

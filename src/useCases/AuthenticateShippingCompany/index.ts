import { AuthenticateShippingCompanyController } from './AuthenticateShippingCompanyController'
import { AuthenticateShippingCompanyUseCase } from './AuthenticateShippingCompanyUseCase'
import { HashService } from '../../services/HashService'
import { SequelizeShippingCompanyRepository } from '../../repositories/implementations/SequelizeShippingCompanyRepository'
import { TokenService } from '../../services/TokenService'

const hashService = new HashService()

const sequelizeShippingCompanyRepository =
  new SequelizeShippingCompanyRepository()

const tokenService = new TokenService()

const authenticateShippingCompanyUseCase =
  new AuthenticateShippingCompanyUseCase(
    hashService,
    sequelizeShippingCompanyRepository,
    tokenService
  )

const authenticateShippingCompanyController =
  new AuthenticateShippingCompanyController(authenticateShippingCompanyUseCase)

export {
  authenticateShippingCompanyUseCase,
  authenticateShippingCompanyController,
}

import { Request, Response } from 'express'
import { AuthenticateShippingCompanyUseCase } from './AuthenticateShippingCompanyUseCase'

export class AuthenticateShippingCompanyController {
  constructor(
    private authenticateShippingCompanyUseCase: AuthenticateShippingCompanyUseCase
  ) {}

  async execute(request: Request, response: Response): Promise<Response> {
    const { cnpj, password } = request.body

    try {
      await this.authenticateShippingCompanyUseCase.execute({
        cnpj,
        password,
      })

      return response.status(201).json('User is authenticated with success')
    } catch (err) {
      return response.status(400).json({
        message: err.message || 'Something went wrong',
      })
    }
  }
}

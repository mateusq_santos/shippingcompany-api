import 'dotenv/config'
import { app } from './app'
import { db } from './database/db'

app.listen(3000, async () => {
  await db.sync().then(
    function () {
      console.log('DB connection successful.')
    },
    function (err) {
      console.log(err)
    }
  )
  console.log('Server is running on port 3000')
})

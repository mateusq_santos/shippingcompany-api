import * as yup from 'yup'

export const shippingCompanyValidator = yup.object().shape({
  cnpj: yup
    .string()
    .min(14)
    .max(14)
    .required('O CNPJ precisa ter 14 caracteres'),
  name: yup.string().required(),
  corporateName: yup.string().required(),
  address: yup.string().required(),
  stateRegistration: yup
    .string()
    .min(9)
    .max(9)
    .required('A inscrição estadual precisa ter 9 caracteres'),
  contact: yup.string().required(),
  password: yup
    .string()
    .min(6)
    .required('A senha deve ter pelo menos 6 caracteres'),
})

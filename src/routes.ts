import { Router } from 'express'
import { createShippingCompanyController } from './useCases/CreateShippingCompany'
import { authenticateShippingCompanyController } from './useCases/AuthenticateShippingCompany'

const routes = Router()

routes.post('/register', (request, response) => {
  return createShippingCompanyController.execute(request, response)
})

routes.post('/auth', (request, response) => {
  return authenticateShippingCompanyController.execute(request, response)
})

export { routes }

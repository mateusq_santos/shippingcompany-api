import { ShippingCompany } from '../entities/ShippingCompany'

export interface ShippingCompanyRepository {
  findByCNPJ(cnpj: string)

  save(ShippingCompany: ShippingCompany)
}

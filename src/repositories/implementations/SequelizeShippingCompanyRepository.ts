import { ShippingCompanyRepository } from '../ShippingCompanyRepository'
import ShippingCompanyModel from '../../database/models/ShippingCompanyModel'
import { CreateShippingCompanyRequestDTO } from '../../useCases/CreateShippingCompany/CreateShippingCompanyDTO'

export class SequelizeShippingCompanyRepository
  implements ShippingCompanyRepository
{
  async findByCNPJ(cnpj: string) {
    const result = await ShippingCompanyModel.findOne({
      where: {
        cnpj,
      },
    })

    return result
  }

  async save(data: CreateShippingCompanyRequestDTO) {
    const result = await ShippingCompanyModel.create({
      name: data.name,
      contact: data.contact,
      corporateName: data.corporateName,
      cnpj: data.cnpj,
      address: data.address,
      password: data.password,
      stateRegistration: data.stateRegistration,
    })

    return result
  }
}

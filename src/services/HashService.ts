import * as bcrypt from 'bcrypt'

export class HashService {
  get SALT_ROUNDS() {
    return Number(process.env.SALT_ROUNDS) || 16
  }

  async hash(value: string): Promise<string> {
    return bcrypt.hash(value, this.SALT_ROUNDS)
  }

  async compare(value: string, hash: string): Promise<boolean> {
    return bcrypt.compare(value, hash)
  }
}

import jwt from 'jsonwebtoken'

interface ITokenPayload {
  sub: string
}

export class TokenService {
  async generate(userId: string): Promise<string> {
    const payload: ITokenPayload = { sub: userId }
    return jwt.sign(payload, process.env.JWT_SECRET)
  }

  async verify(payload: string) {
    return jwt.verify(payload, process.env.JWT_SECRET)
  }
}

import { db } from '../db'
import { ShippingCompany } from '../../entities/ShippingCompany'
import { DataTypes, Model, Optional, Sequelize } from 'sequelize'
import { v4 as uuid_v4 } from 'uuid'

export interface ShippingCompanyModelInput
  extends Optional<ShippingCompany, 'id'> {}

export interface ShippingCompanyModelOutput extends Required<ShippingCompany> {}
class ShippingCompanyModel
  extends Model<ShippingCompany, ShippingCompanyModelInput>
  implements ShippingCompany
{
  public id!: string

  public cnpj!: string

  public name!: string

  public corporateName!: string

  public address!: string

  public stateRegistration!: string

  public contact!: string

  public password!: string
}

ShippingCompanyModel.init(
  {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
      defaultValue: uuid_v4(),
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    cnpj: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    corporateName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stateRegistration: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contact: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  { sequelize: db }
)

export default ShippingCompanyModel

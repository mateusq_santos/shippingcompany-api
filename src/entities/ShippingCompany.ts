export class ShippingCompany {
  public readonly id: string

  public cnpj: string

  public name: string

  public corporateName: string

  public address: string

  public stateRegistration: string

  public contact: string

  public password: string
}
